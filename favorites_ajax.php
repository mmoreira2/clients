<?php
//
// Favorites Menu Component
// Copyright (c) 2020-2021 Nagios Enterprises, LLC. All rights reserved.
//

require_once(dirname(__FILE__) . '/../../../config.inc.php');
require_once(dirname(__FILE__) . '/../../common.inc.php');
require_once(dirname(__FILE__) . '/../componenthelper.inc.php');

// Initialization stuff
pre_init();
init_session();

// Grab GET or POST variables, check prereqs, and authorization
grab_request_vars();
check_prereqs();
check_authentication(false);

$mode = grab_request_var('mode');
switch ($mode) {
    case 'add':
        menu_favorites_add_favorite();
        break;
    case 'edit':
        menu_favorites_edit_favorite();
        break;
}

function menu_favorites_add_favorite()
{
    global $db_tables;

    $user_id = $_SESSION["user_id"];
    $title = grab_request_var('title', '');
    $partial_href = grab_request_var('partial_href', '');

    if (empty($title)) {
        $title = _('New Favorite Page');
    }

    if (empty($partial_href)) {
        $partial_href = 'index.php';
    }

    $title = escape_sql_param($title, DB_NAGIOSXI);
    $partial_href = escape_sql_param($partial_href, DB_NAGIOSXI);

    $sql = sprintf("INSERT INTO " . $db_tables[DB_NAGIOSXI]['cmp_favorites'] ." (user_id, title, partial_href)
            VALUES (%d, '%s', '%s')", $user_id, $title, $partial_href);
    exec_sql_query(DB_NAGIOSXI, $sql);

    print json_encode(array($user_id, $title, $partial_href));
}
 
function menu_favorites_edit_favorite()
{
    global $db_tables;

    $item = grab_request_var('item', array());
    $user_id = $_SESSION["user_id"];

    // All of these are duplicate keys - we're just updating titles
    $update_sql = "INSERT INTO " . $db_tables[DB_NAGIOSXI]['cmp_favorites'] . " (item_id, title, user_id, partial_href) 
    VALUES %s 
    ON DUPLICATE KEY 
        UPDATE item_id=VALUES(item_id), title=VALUES(title)";

    $delete_sql = "DELETE FROM " . $db_tables[DB_NAGIOSXI]['cmp_favorites'] . " WHERE %s";

    foreach ($item as $item_id => $title) {
        $item_id = intval($item_id);
        $title = escape_sql_param($title, DB_NAGIOSXI);

        if ($title !== '*DELETED*') {
            // Add a clause to the update statement
            $update_sql = sprintf($update_sql, " ($item_id, '$title', '$user_id', ''), %s ");
            continue;    
        }
        // Add a clause to the delete statement.
        $delete_sql = sprintf($delete_sql, "item_id = $item_id OR %s ");
    }

    $update_sql = str_replace(', %s', '', $update_sql);
    $delete_sql = str_replace('OR %s', '', $delete_sql);

    if (strpos($update_sql, '%s') === false) {
        exec_sql_query(DB_NAGIOSXI, $update_sql);
    }

    if (strpos($delete_sql, '%s') === false) {
        exec_sql_query(DB_NAGIOSXI, $delete_sql);
    }
}
